<?php

namespace laracastsite\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $tasks = [
            'Mala kornið',
            'Baka brauðið',
            'Vera algjör montrass',
        ];

        return view('welcome', [
            'tasks' => $tasks,
        ]);
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }
}
