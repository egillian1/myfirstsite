<?php

namespace laracastsite\Http\Controllers;

use Illuminate\Http\Request;
use laracastsite\Task;
use laracastsite\Project;

class ProjectTasksController extends Controller
{
    public function store(Project $project) {
        $attributes = request()->validate(['description' => 'required']);

        $project->addTask($attributes);

        return back();
    }

    public function update(Task $task) {
        $task->complete();

        /*
        $task->update([
            'completed' => request()->has('completed')
        ]);
        */

        return back();
    }
}
