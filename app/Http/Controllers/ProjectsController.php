<?php

namespace laracastsite\Http\Controllers;

use Illuminate\Http\Request;
use laracastsite\Project;

class ProjectsController extends Controller
{
    public function construct() {
        // Apply authentication to all methods in the class
        $this->middleware('auth');
    }

    public function index() {
        return view('projects.index', [
            'projects' => auth()->user()->projects,
        ]);
    }

    public function show(Project $project) {

        return view('projects.show', [
            'project' => $project,
        ]);
    }

    public function create() {
        return view('projects.create');
    }

    public function store() {
        $attributes = $this->validateProject();

        $attributes['owner_id'] = auth()->id();

        Project::create($attributes);

        session()->flash('message', 'Your project has been created');

        return redirect('/projects');
    }

    public function edit(Project $project) {
        return view('projects.edit', [
            'project' => $project,
        ]);
    }

    public function update(Project $project) {
        $this->authorize('update', $project);

        $project->update($this->validateProject());

        return redirect('/projects');
    }

    public function destroy(Project $project) {
        $this->authorize('update', $project);

        $project->delete();

        return redirect('/projects');
    }

    protected function validateProject() {
        return request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
    }
}
