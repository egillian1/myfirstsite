<?php

namespace laracastsite\Policies;

use laracastsite\User;
use laracastsite\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the project.
     *
     * @param  \laracastsite\User  $user
     * @param  \laracastsite\Project  $project
     * @return mixed
     */
    public function view(User $user, Project $project)
    {
        return $project->owner_id == $user->id;
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param  \laracastsite\User  $user
     * @param  \laracastsite\Project  $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        return $project->owner_id == $user->id;
    }
}
