<?php

namespace laracastsite;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title',
        'description',
        'owner_id',
    ];

    protected static function boot() {
        parent::boot();

        static::created(function($project) {
            // Event fired when new Project created
        });
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }

    public function addTask($task) {
        $this->tasks()->create($task);
    }
}
