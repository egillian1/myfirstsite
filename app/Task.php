<?php

namespace laracastsite;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'completed',
        'description',
        'project_id'
    ];

    public function complete($completed = true) {
        $this->update(['completed' => $completed]);
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }
}
