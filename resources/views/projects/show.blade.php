@extends('layout')

@section('content')
    <h1 class="title">{{ $project->title }}</h1>

    <div class="content">{{ $project->description }}</div>

    <p>
        <a href="/projects/{{ $project->id }}/edit">Edit</a>
    </p>

    @if($project->tasks->count())
        <div class="box">
            @foreach($project->tasks as $task)
                <form method="POST" action="/tasks/{{ $task->id }}">
                    @csrf
                    @method('PATCH')

                    <label class="checkbox" for="completed">
                        <input type="checkbox" name="completed" onChange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
                        {{ $task->description }}
                    </label>
                </form>
            @endforeach
        </div>
    @endif

    {{-- Form for adding new tasks --}}

    <form class="box" method="POST" action="/projects/{{ $project->id }}/tasks">
        @csrf

        <div class="field">
            <label class="label" for="description">New Task</label>

            <div class="control">
                <input type="text" class="input" name="description" placeholder="Description">
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Add Task</button>
            </div>
        </div>

        @include('errors')
    </form>

@endsection
