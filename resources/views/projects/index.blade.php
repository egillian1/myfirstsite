@extends('layout')

@section('content')
    <h1 class="title">Projects</h1>

    <div class="content">
        <ul>
            @foreach($projects as $project)
                @can('view', $project)
                    <a href="/projects/{{ $project->id }}">
                        <li>{{ $project->title }}</li>
                    </a>
                @endcan
            @endforeach
        </ul>
    </div>

    <a href="/projects/create">Create New Project</a>

    @if (session('message'))
        <div>{{ session('message') }}</div>
    @endif

@endsection
