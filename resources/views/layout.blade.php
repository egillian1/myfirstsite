<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'firstsite')</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>

<body>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about">About us</a></li>
        <li><a href="/contact">Contact us</a></li>
    </ul>

    <div class="container">
        @yield('content')
    </div>

    <script src="/js/app.js"></script>
</body>
</html>
