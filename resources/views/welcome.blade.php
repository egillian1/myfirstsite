@extends('layout')

@section('title', 'Welcome')

@section('content')
    <h1>First Site</h1>

    <div id="app">
        <example-component></example-component>
    </div>

    <ul>
        @foreach ($tasks as $task)
            <li>{{ $task }}</li>
        @endforeach
    </ul>

@endsection
